package com.example.safe;

public class Main {

	public static void main(String[] args) {
		Bars bars = new Bars();
		SoundAlert sound = new SoundAlert();
		Police police = new Police();
		Dogs dogs = new Dogs();
		
		
		Alarm alarm = new Alarm("1234");
		
		alarm.addListener(dogs);
		alarm.addListener(police);
		alarm.addListener(sound);
		alarm.addListener(bars);

	}

}
