package com.example.safe;

import java.util.ArrayList;
import java.util.List;

public class Alarm {
	private List<AlarmListener> listeners;
	private String pin;
	
	private Thread some = new Thread()
		{
			public void run()
			{
				while(true){
					int r = (int)(Math.random()*2);
					switch(r)
					{
					case 0: correctEnteredPin();break;
					case 1: wrongEnteredPin();break;
					}
					try {
						sleep(3000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

				}
					
			}
				
		};
	
	
	public Alarm(String pin){
		this.pin = pin;
		this.listeners = new ArrayList<AlarmListener>();
		some.start();
	}
	
	public synchronized void addListener(AlarmListener listener){
		listeners.add(listener);
	}
	
	public synchronized void removeListener(AlarmListener listener){
		listeners.remove(listener);
	}
	
	public synchronized void enterPin(String pin){
		this.pin = pin;
	}
	
	protected synchronized void wrongEnteredPin(){
		System.out.println("Wrong Pin\nTurning the alarm.");
		EnteredPinEvent e =  new EnteredPinEvent(this);
		for(AlarmListener l : this.listeners)
			l.alarmTurnedOn(e);
	}
	
	protected synchronized void correctEnteredPin(){
		System.out.println("Opening");
		EnteredPinEvent e =  new EnteredPinEvent(this);
		for(AlarmListener l : this.listeners)
			l.alarmTurnedOff(e);
	}

}
