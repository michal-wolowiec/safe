package com.example.safe;

public class SoundAlert implements AlarmListener{

	@Override
	public void alarmTurnedOn(EnteredPinEvent event) {
		System.out.println("Sound ON");
		
	}

	@Override
	public void alarmTurnedOff(EnteredPinEvent event) {
		System.out.println("Sound: Stand By");
		
	}

}
