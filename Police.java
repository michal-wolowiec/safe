package com.example.safe;

public class Police implements AlarmListener{

	@Override
	public void alarmTurnedOn(EnteredPinEvent event) {
		System.out.println("Police on their way!!");
		
	}

	@Override
	public void alarmTurnedOff(EnteredPinEvent event) {
		System.out.println("Police: Stand By");
		
	}
	
}
