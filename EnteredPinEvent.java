package com.example.safe;

import java.util.Date;

public class EnteredPinEvent {
	private Alarm alarm;
	private Date eventDate;
	
	public EnteredPinEvent(Alarm alarm){
		this.alarm = alarm;
		this.eventDate = new Date();
	}
	
	public Alarm getAlarm(){
		return this.alarm;
	}
}
